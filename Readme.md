SphereBob
---

SphereBob is **node-webkit** project to view google's photosphere images.

**node-webkit** is an app runtime based on ```Chromium``` and ```node.js```.

> You can write native apps in HTML and Javascript with node-webkit. It also lets you to call Node.js modules directly from DOM and enables a new way of writing native applications with all Web technologies.

>**Features**

>* Apps written in modern HTML5, CSS3, JS and WebGL.
* Complete support for Node.js APIs and all its third party modules.
* Good performance: Node and WebKit runs in the same thread: Function calls are made straightforward;
* objects are in the same heap and can just reference each other;
* Easy to package and distribute apps.
* Available on Linux, Mac OSX and Windows

> [source] [1]

Tech
-
SphereBob uses a number of open source projects to work properly:

* [node-webkit] - v0.8.3-win32
* [three] - Three.js

Version
-
0.1

Windows versions
-
Tested on `Windows 7 32-bit & 64-bit`.

Quick start
-

**Install**

`git clone https://gitlab.com/UdeF/spherebob.git spherebob`

**Compile**

for Windows-systems with [nodebob]:

`git clone https://github.com/geo8bit/nodebob.git nodebobPhotosphere`

cd nodebobPhotosphere

rd app /s /q

`git clone https://gitlab.com/UdeF/spherebob.git app`

Execute `build.bat`

Thanks to:
-

@kennydude

http://stackoverflow.com/questions/1578169/how-can-i-read-xmp-data-from-a-jpg-with-php




License
-
MIT

[node-webkit]: https://github.com/rogerwang/node-webkit
[three]: https://github.com/mrdoob/three.js
[nodebob]: https://github.com/geo8bit/nodebob
[1]: https://github.com/rogerwang/node-webkit#introduction